const fs = require('fs');
const baiduScraper = require('./scrapers/baiduScraper');

(async () => {
  try {
    keyword = 'M2增速';
    csv = await baiduScraper(keyword);
    filename = `./results/百度新闻-${keyword}.csv`;
    fs.writeFileSync(filename, csv);
  } catch (err) {
    console.log('\x1b[31m%s\x1b[0m', err);
  }
})();
