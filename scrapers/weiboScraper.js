const puppeteer = require('puppeteer');
const json2csv = require('json2csv').parse;
const winston = require('winston');
const logger = require('../logger');

logger.add(new winston.transports.File({ filename: 'logs/weibo-scraper.log' }));

async function weiboScraper(keyword) {
  const browser = await puppeteer.launch({
    headless: false,
    args: [
      '--no-sandbox',
      '--disable-setuid-sandbox',
      '--disable-dev-shm-usage',
      '--disable-accelerated-2d-canvas',
      '--disable-gpu',
      '--window-size=1920x1080',
    ],
  });
  const page = await browser.newPage();
  await page.setRequestInterception(true);
  page.on('request', req => {
    if (['stylesheet', 'font', 'image'].includes(req.resourceType())) {
      req.abort();
    } else {
      req.continue();
    }
  });

  let url = `https://s.weibo.com/weibo?q=${keyword}`;
  let pageNum = 1;
  let results = [];

  while (url) {
    logger.info(`[Scraping page ${pageNum}] ${url}`);

    const response = await page.goto(url, { timeout: 0, waitUntil: 'domcontentloaded' });

    if (response.status() !== 200 || response.request().redirectChain().length) {
      logger.warn(`Failed for some reason. Scraping again...`);
      continue;
    }

    // await page.screenshot({ path: `./screenshots/微博搜索-p${pageNum}.png`, fullPage: true });

    const newResults = await page.evaluate(() =>
      Array.from(document.querySelectorAll('.card-feed')).map(result => {
        const $time = result.querySelector('.from a:first-child');
        const $excerpt = result.querySelector('.txt');
        const $source = result.querySelector('.info .name');
        return {
          time: $time.textContent.trim(),
          source: $source.textContent.trim(),
          title: $excerpt.textContent.trim().substring(0, 15) + '...',
          link: $time.href,
        };
      })
    );

    logger.info(`[Page ${pageNum}] found ${newResults.length} results`);

    results = results.concat(newResults);

    url = await page.evaluate(() => {
      const $nextLink = document.querySelector('.m-page .next');
      return $nextLink ? $nextLink.href : null;
    });

    pageNum++;
  }

  await browser.close();

  logger.info(`[Finished scraping for ${keyword}] found ${results.length} results`);

  const csv = json2csv(results, {
    fields: [
      { label: '时间', value: 'time' },
      { label: '来源', value: 'source' },
      { label: '标题', value: 'title' },
      { label: '链接', value: 'link' },
    ],
    withBOM: true,
  });

  return csv;
}

module.exports = weiboScraper;
