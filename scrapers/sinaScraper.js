const puppeteer = require('puppeteer');
const json2csv = require('json2csv').parse;
const winston = require('winston');
const logger = require('../logger');

logger.add(new winston.transports.File({ filename: 'logs/sina-scraper.log' }));

async function sinaScraper(keyword) {
  const browser = await puppeteer.launch({
    args: [
      '--no-sandbox',
      '--disable-setuid-sandbox',
      '--disable-dev-shm-usage',
      '--disable-accelerated-2d-canvas',
      '--disable-gpu',
      '--window-size=1920x1080',
    ],
  });
  const page = await browser.newPage();
  await page.setRequestInterception(true);
  page.on('request', req => {
    if (['stylesheet', 'font', 'image'].includes(req.resourceType())) {
      req.abort();
    } else {
      req.continue();
    }
  });

  let url = `http://search.sina.com.cn/?c=news&q=${keyword}&range=all&num=20&col=`;
  let pageNum = 1;
  let results = [];

  while (url) {
    logger.info(`[Scraping page ${pageNum}] ${url}`);

    const response = await page.goto(url, { timeout: 0 });

    if (response.status() !== 200 || response.request().redirectChain().length) {
      logger.warn(`Failed for some reason. Scraping again...`);
      continue;
    }

    await page.screenshot({ path: `./screenshots/新浪新闻-p${pageNum}.png`, fullPage: true });

    const newResults = await page.evaluate(() =>
      Array.from(document.querySelectorAll('.box-result')).map(result => {
        const $title = result.querySelector('h2 a');
        const $meta = result.querySelector('h2 .fgray_time');
        const metaArr = $meta.textContent.trim().split(/ (.+)/);
        return {
          time: metaArr[1],
          source: metaArr[0],
          title: $title.textContent.trim(),
          link: $title.href,
        };
      })
    );

    logger.info(`[Page ${pageNum}] found ${newResults.length} results`);

    results = results.concat(newResults);

    url = await page.evaluate(() => {
      const $nextLink = document.querySelector('.pagebox a[title="下一页"]');
      return $nextLink ? $nextLink.href : null;
    });

    pageNum++;

    if (pageNum > 30) break;
  }

  await browser.close();

  logger.info(`[Finished scraping for ${keyword}] found ${results.length} results`);

  const csv = json2csv(results, {
    fields: [
      { label: '时间', value: 'time' },
      { label: '来源', value: 'source' },
      { label: '标题', value: 'title' },
      { label: '链接', value: 'link' },
    ],
    withBOM: true,
  });

  return csv;
}

module.exports = sinaScraper;
