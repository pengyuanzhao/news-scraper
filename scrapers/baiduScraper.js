const puppeteer = require('puppeteer');
const json2csv = require('json2csv').parse;
const winston = require('winston');
const logger = require('../logger');

logger.add(new winston.transports.File({ filename: 'logs/baidu-scraper.log' }));

async function baiduScraper(keyword) {
  const browser = await puppeteer.launch({
    headless: false,
    args: [
      '--no-sandbox',
      '--disable-setuid-sandbox',
      '--disable-dev-shm-usage',
      '--disable-accelerated-2d-canvas',
      '--disable-gpu',
      '--window-size=1920x1080',
    ],
  });
  const page = await browser.newPage();
  await page.setRequestInterception(true);
  page.on('request', req => {
    if (['stylesheet', 'font', 'image'].includes(req.resourceType())) {
      req.abort();
    } else {
      req.continue();
    }
  });

  let url = `http://news.baidu.com/ns?word=${keyword}&sr=0&cl=2&pn=0&rn=20&tn=newsdy&ct=0&clk=sortbytime`;
  let pageNum = 1;
  let results = [];

  while (url) {
    logger.info(`[Scraping] ${url}`);

    const response = await page.goto(url, { timeout: 0 });

    if (response.status() !== 200 || response.request().redirectChain().length) {
      logger.warn(`Failed for some reason. Scraping again...`);
      continue;
    }

    // await page.screenshot({ path: `./screenshots/百度新闻-p${pageNum}.png`, fullPage: true });

    const newResults = await page.evaluate(() =>
      Array.from(document.querySelectorAll('.result')).map(result => {
        const $title = result.querySelector('.c-title a');
        const $meta = result.querySelector('.c-author');
        const metaArr = $meta.textContent.trim().split(/\s+/);
        return {
          date: metaArr[1],
          time: metaArr[2],
          source: metaArr[0],
          title: $title.textContent.trim(),
          link: $title.href,
        };
      })
    );

    logger.info(`[Page ${pageNum}] found ${newResults.length} results`);
    console.log(' ');

    results = results.concat(newResults);

    url = await page.evaluate(() => {
      const $nextLink = document.querySelector('#page a:last-child.n');
      return $nextLink ? $nextLink.href : null;
    });

    pageNum++;
  }

  await browser.close();

  logger.info(`[Finished scraping for ${keyword}] found ${results.length} results`);

  const csv = json2csv(results, {
    fields: [
      { label: '日期', value: 'date' },
      { label: '时间', value: 'time' },
      { label: '来源', value: 'source' },
      { label: '标题', value: 'title' },
      { label: '链接', value: 'link' },
    ],
    withBOM: true,
  });

  return csv;
}

module.exports = baiduScraper;
