import React from 'react';
import styled, { createGlobalStyle } from 'styled-components';
import saveAs from 'file-saver';
import { Button, Card, Loading, Input, Select } from 'element-react';

const GlobalStyle = createGlobalStyle`
  body {
    font-family: "Helvetica Neue",Helvetica,"PingFang SC",
      "Hiragino Sans GB","Microsoft YaHei","微软雅黑",Arial,sans-serif;
    font-size: 14px;
  }
`;

const Wrapper = styled.div`
  width: 100%;
  max-width: 480px;
  margin: 24px auto;
  > .el-card {
    margin-bottom: 16px;
  }
  form {
    > .el-select {
      width: 100%;
      margin-bottom: 16px;
    }
    > .el-input {
      margin-bottom: 16px;
    }
  }
`;

const Results = styled.ul`
  margin: 0;
  padding: 0;
  li {
    display: flex;
    align-items: center;
    justify-content: space-between;
    + li {
      margin-top: 8px;
      padding-top: 8px;
      border-top: 1px solid #e0e6ed;
    }
  }
`;

const sites = [
  { label: '百度新闻', value: 'baidu' },
  { label: '新浪财经', value: 'sina' },
  { label: '微博搜索', value: 'weibo' },
];

class App extends React.Component {
  state = {
    loading: false,
    keyword: '',
    source: '',
    results: [],
  };

  onSubmit = async () => {
    const { keyword, source } = this.state;
    const url = `/api/scraper?source=${source}&keyword=${keyword}`;

    this.setState({ loading: true });

    try {
      const res = await fetch(url).then(res => res.json());
      this.setState(state => ({
        loading: false,
        results: [...state.results, res],
      }));
    } catch (err) {
      console.log(err);
    }
  };

  onChangeKeyword = keyword => {
    this.setState({ keyword });
  };

  onSelectSource = source => {
    this.setState({ source });
  };

  onSaveFile(resultIndex) {
    const { csv, filename } = this.state.results[resultIndex];
    const blob = new Blob([csv], { type: 'text/plain;charset=utf-8' });
    saveAs(blob, filename);
  }

  render() {
    const { loading, keyword, source, results } = this.state;

    return (
      <Wrapper>
        <GlobalStyle />
        {results.length > 0 && (
          <Card>
            <Results>
              {results.map(({ filename }, index) => (
                <li key={index}>
                  <span>{filename}</span>
                  <Button size="small" onClick={() => this.onSaveFile(index)}>
                    下载
                  </Button>
                </li>
              ))}
            </Results>
          </Card>
        )}
        <Loading text="正在爬..." loading={loading}>
          <form>
            <Input placeholder="请输入关键字" value={keyword} onChange={this.onChangeKeyword} />
            <Select placeholder="请选择网站" value={source} onChange={this.onSelectSource}>
              {sites.map(site => (
                <Select.Option key={site.value} label={site.label} value={site.value} />
              ))}
            </Select>
            <Button type="primary" onClick={this.onSubmit}>
              开始搜索
            </Button>
          </form>
        </Loading>
      </Wrapper>
    );
  }
}

export default App;
