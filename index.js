const path = require('path');
const express = require('express');

const app = express();
const port = process.env.PORT || 3001;
const scrapers = {
  baidu: { scraper: require('./scrapers/baiduScraper'), name: '百度新闻' },
  sina: { scraper: require('./scrapers/sinaScraper'), name: '新浪财经' },
  weibo: { scraper: require('./scrapers/weiboScraper'), name: '微博搜索' },
};

app.use(express.static(path.join(__dirname, '/client/build')));

app.get('/api/scraper', async (req, res) => {
  console.log('new request ...........................');
  // req.connection.setTimeout(1000 * 60 * 30); // ten minutes

  try {
    const { source, keyword } = req.query;
    const scraper = scrapers[source];

    if (!scraper) {
      return res.status(400).json({ error: 'Unsuportted source' });
    }

    const csv = await scraper.scraper(keyword);
    const filename = `${scraper.name}-${keyword}.csv`;

    return res.json({ csv, filename });
  } catch (error) {
    return res.status(500).json({ error });
  }
});

app.get('*', (req, res) => {
  res.sendFile(path.join(__dirname, `/client/build/index.html`));
});

const server = app.listen(port, () => {
  console.log(`Server listening on port ${port}`);
});

server.timeout = 1000 * 60 * 10;
